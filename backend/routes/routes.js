const express = require("express");
const router = express.Router();
const postController = require('../controller/todo');
const checkAuth = require('../middleware/checkAuth');

router.post('',  postController.createpost);
router.get('', postController.getpost);
router.get('/:id', checkAuth, postController.getpostById);
router.put('/:id', checkAuth, postController.updatedPost);
router.delete('/:id', checkAuth, postController.deletePost)

module.exports = router