const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/user');

exports.createUser = (req, res, next) => {

    bcrypt.hash(req.body.password, 10).then(hash => {
        const user = new User({
            name: req.body.name,
            email: req.body.email,
            password: hash
        });
        user.save().then(result => {
            res.status(201).json({
                message: 'User Added Successfully',
                user: result
            });
        }).catch(err => {
            res.status(501).json({
                error: err
            })
        })
    })
}

exports.login = (req, res, next) => {
    // console.log(req.body);
    // return
    let fetchedUser;

    User.findOne({ email: req.body.email })
        .then(user => {
            if (!user) {
                return res.status(402).json({
                    message: 'Invalid Email And Password'
                });
            }
            fetchedUser = user;
            return bcrypt.compare(req.body.password, user.password);


        })
        .then(result => {
            if (!result) {
                return res.status(401).json({
                    message: 'Invalid Email And Password'
                });
            }
            const token = jwt.sign({ email: fetchedUser.email, userId: fetchedUser._id }, 'secret_this_should_be_longer', {
                expiresIn: '1h',
            });
            res.status(200).json({
                token: token,
                expiresIn: 60,
                userId: fetchedUser._id
            });
        }).catch(err => {
            return res.status(404).json({
                message: 'Auth Failed'
            })
        })
}