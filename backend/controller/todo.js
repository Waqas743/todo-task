const Todo = require('../models/todo');

exports.createpost = (req, res, next) => {
    console.log(req.body);
    return
    const todo = new Todo({
        title: req.body.title,
        content: req.body.content,
        // creator: req.userData.userId
    });
    todo.save().then(result => {
        res.status(201).json({
            message: 'Todo Added Successfully',
            postId: result._id
        });
    });
}

exports.getpost = (req, res, next) => {
    Todo.find().then(document => {
        res.status(200).json({
            message: 'Todo fetched successfully',
            posts: document
        });
    });
}

exports.getpostById = (req, res, next) => {
    Todo.findById(req.params.id).then(result => {
        if (result) {
            res.status(200).json(result);
        } else {
            res.status(404).json({
                message: 'Page Not Found'
            })
        }
    })
}


exports.updatedPost = (req, res, next) => {
    const post = new Todo({
        _id: req.body.id,
        title: req.body.title,
        content: req.body.content,
        creator: req.userData.userId
    });
    Todo.updateOne({ _id: req.body.id, creator: req.userData.userId }, post).then(result => {
        if (result.nModified > 0) {
            res.status(200).json({
                message: 'Update Data'
            })
        } else {
            res.status(401).json({
                message: 'Auth Failed'
            })
        }
        res.status(200).json({
            message: 'Todo Updated Successfully',
            // updatedposts: result
        });
    });
}

exports.deletePost = (req, res, next) => {
    Todo.deleteOne({ _id: req.params.id, creator: req.userData.userId }).then(result => {
        if (result.n > 0) {
            res.status(200).json({
                message: 'Delete data'
            })
        } else {
            res.status(401).json({
                message: 'Auth Failed'
            })
        }
        res.status(200).json({
            message: 'Post Deleted!'
        })
    })
}