const mongoose = require('mongoose');
const todoSchema = mongoose.Schema({
    title: { type: String, required: true },
    completed: { type: Boolean, required: true },
    creator: { type: mongoose.Schema.Types.ObjectId, ref: 'user', required: true }
})
module.exports = mongoose.model('todo', todoSchema);