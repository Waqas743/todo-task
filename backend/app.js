const express = require("express");
const bodyparser = require("body-parser");
const postRoutes = require('./routes/routes');
const userRoutes = require('./routes/user');
const mongoose = require('mongoose');
const app = express();

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, PUT, DELETE, OPTION"
    );
    next();
});
mongoose.connect("mongodb://localhost:27017/check", { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true })
    .then(() => {
        console.log('connected to database')
    })
    .catch(() => {
        console.log('Connection Failed')
    });

app.use('/api/posts', postRoutes);
app.use('/api/user', userRoutes);
module.exports = app